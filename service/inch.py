from requests import get
from .mexc import MexcService
from .kucoin import KucoinService
from .gate import GateService
from .bitrue import BitrueService
from .okx import OkxService
import json
from datetime import datetime
from os import getcwd


def get_price(amount, to_):
    count = int(to_) / 1000000000000000000
    return amount / count


def compare(first, sec):
    return float(first) / float(sec)


def get_result(pair, from_, to_):
    comp = compare(from_, to_)
    avg = (comp * 100 - 100)
    return {
        "pair": pair,
        "price_from": from_,
        "price_to": to_,
        "compare": avg
    }


class InchService:
    def __init__(self, data):
        self.data = data
        self.url = "https://api.1inch.io/v4.0/1/quote"
        self.back_data = {}
        self.mexc = MexcService()
        self.kucoin = KucoinService()
        self.gate = GateService()
        self.bitrue = BitrueService()
        self.okx = OkxService()
        self.start()

    def start(self):
        x = 1
        xx = len(self.data) + 1
        while True:
            for to_key, to_value in self.data.items():
                # print(f"{x} из {xx}")
                x += 1

                amount = 1
                PARAMS = {
                    "fromTokenAddress": "0xdac17f958d2ee523a2206206994597c13d831ec7",
                    "toTokenAddress": to_key,
                    "amount": f"{1000000 * amount}",
                }
                from_ = "USDT"
                to = f"{to_value.get('symbol')}"
                resp = get(self.url, params=PARAMS)

                inch_data = resp.json()
                # print(inch_data)
                if inch_return := inch_data.get("toTokenAmount"):
                    inch_price = get_price(amount, inch_data.get("toTokenAmount"))
                    self.back_data[f"{from_}_{to}"] = {"inch": inch_price}
                #print(f"Inch {from_}_{to}",
                #      "Amount: ", self.back_data[f"{from_}_{to}"],
                #      "Price: ", (1 / self.back_data[f"{from_}_{to}"]['inch']))

                mexc_data_from = self.mexc.start(f"{to}", f"{from_}")
                if mexc_return := mexc_data_from.get("data"):
                    comp = get_result(f"{to}_{from_}", inch_price, mexc_return.get("bids")[0]["price"])
                    if self.back_data.get(f"{to}_{from_}"):
                        self.back_data[f"{to}_{from_}"].update({"mexc": comp})
                    else:
                        self.back_data[f"{to}_{from_}"] = {"mexc": comp}
                    print(f"Mexc from:", self.back_data[f"{to}_{from_}"])

                mexc_data_to = self.mexc.start(f"{from_}", f"{to}")
                if mexc_return := mexc_data_to.get("data"):
                    comp = get_result(f"{from_}_{to}", inch_price, mexc_return.get("asks")[0]["price"])
                    if self.back_data.get(f"{from_}_{to}"):
                        self.back_data[f"{from_}_{to}"].update({"mexc": comp})
                    else:
                        self.back_data[f"{from_}_{to}"] = {"mexc": comp}
                    print(f"Mexc to:", self.back_data[f"{from_}_{to}"])

                kucoin_data_to = self.kucoin.start(from_, to)
                if kucoin_return_to := kucoin_data_to.get("data"):
                    comp = get_result(f"{from_}_{to}", inch_price, kucoin_return_to.get("bestAsk"))
                    if self.back_data.get(f"{from_}_{to}"):
                        self.back_data[f"{from_}_{to}"].update({"kucoin": comp})
                    else:
                        self.back_data[f"{from_}_{to}"] = {"kucoin": comp}
                    print(f"Kukoin to:", self.back_data[f"{from_}_{to}"])

                kucoin_data_from = self.kucoin.start(to, from_)
                if kucoin_return_from := kucoin_data_from.get("data"):
                    comp = get_result(f"{to}_{from_}", inch_price, kucoin_return_from.get("bestBid"))
                    if self.back_data.get(f"{to}_{from_}"):
                        self.back_data[f"{to}_{from_}"].update({"kucoin": comp})
                    else:
                        self.back_data[f"{to}_{from_}"] = {"kucoin": comp}
                    print(f"Kukoin from:", self.back_data[f"{to}_{from_}"])

                gate_data_to = self.gate.start(from_, to)
                if gate_return_to := gate_data_to.get("asks"):
                    comp = get_result(f"{from_}_{to}", inch_price, gate_return_to[0][0])
                    if self.back_data.get(f"{from_}_{to}"):
                        self.back_data[f"{from_}_{to}"].update({"gate": comp})
                    else:
                        self.back_data[f"{from_}_{to}"] = {"gate": comp}
                    print(f"Gate to:", self.back_data[f"{from_}_{to}"])

                gate_data_from = self.gate.start(to, from_)
                if gate_return_from := gate_data_from.get("bids"):
                    comp = get_result(f"{to}_{from_}", inch_price, gate_return_from[0][0])
                    if self.back_data.get(f"{to}_{from_}"):
                        self.back_data[f"{to}_{from_}"].update({"gate": comp})
                    else:
                        self.back_data[f"{to}_{from_}"] = {"gate": comp}
                    print(f"Gate from:", self.back_data[f"{to}_{from_}"])

                bitrue_data_to = self.bitrue.start(from_, to)
                if bitrue_return_to := bitrue_data_to.get("askPrice"):
                    comp = get_result(f"{from_}_{to}", inch_price, bitrue_return_to)
                    if self.back_data.get(f"{from_}_{to}"):
                        self.back_data[f"{from_}_{to}"].update({"bitrue": comp})
                    else:
                        self.back_data[f"{from_}_{to}"] = {"bitrue": comp}
                    print(f"Bitrue to:",self.back_data[f"{from_}_{to}"])

                bitrue_data_from = self.bitrue.start(to, from_)
                if bitrue_return_from := bitrue_data_from.get("bidPrice"):
                    comp = get_result(f"{to}_{from_}", inch_price, bitrue_return_from)
                    if self.back_data.get(f"{to}_{from_}"):
                        self.back_data[f"{to}_{from_}"].update({"bitrue": comp})
                    else:
                        self.back_data[f"{to}_{from_}"] = {"bitrue": comp}
                    print(f"Bitrue from:", self.back_data[f"{to}_{from_}"])

                try:
                    okx_data_to = self.okx.start(from_, to)
                    if okx_return_to := okx_data_to.get("data"):
                        comp = get_result(f"{from_}_{to}", inch_price, okx_return_to[0].get("asks")[0][0])
                        if self.back_data.get(f"{from_}_{to}"):
                            self.back_data[f"{from_}_{to}"].update({"okx": comp})
                        else:
                            self.back_data[f"{from_}_{to}"] = {"okx": comp}
                        print(f"Okx to:", self.back_data[f"{from_}_{to}"])

                    okx_data_from = self.okx.start(to, from_)
                    if okx_return_from := okx_data_from.get("data"):
                        comp = get_result(f"{to}_{from_}", inch_price, okx_return_from[0].get("bids")[0][0])
                        if self.back_data.get(f"{to}_{from_}"):
                            self.back_data[f"{to}_{from_}"].update({"okx": comp})
                        else:
                            self.back_data[f"{to}_{from_}"] = {"okx": comp}
                        print(f"Okx from:", self.back_data[f"{to}_{from_}"])

                except Exception as e:
                    print(e)
                    pass
            #if x % 50 == 0:
                #with open(f"{getcwd()}/service/load/{datetime.now()}.json", "w") as fp:
                #    json.dump(self.back_data, fp)
                #self.back_data = {}
        #with open(f"{getcwd()}/load/{datetime.now()}.json", "w") as fp:
        #    json.dump(self.back_data, fp)
        #self.back_data = {}
